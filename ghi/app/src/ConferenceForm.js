import React, { useEffect, useState } from 'react';

function ConferenceForm( ) {

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

          setName('');
          setStartDate('');
          setEndDate('');
          setDescription('');
          setMaxPresentations('');
          setMaxAttendees('');
          setLocation('');
        }
      }


    const [name, setName] = useState('');
    const handleNameChange = (event) => {
      const value = event.target.value;
      setName(value);
    }

    const [start, setStartDate] = useState('');
    const handleStartChange = (event) => {
      const value = event.target.value;
      setStartDate(value);
    }

    const [end, setEndDate] = useState('');
    const handleEndChange = (event) => {
      const value = event.target.value;
      setEndDate(value);
    }

    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
      const value = event.target.value;
      setDescription(value);
    }

    const [maxPresentations, setMaxPresentations] = useState('');
    const handleMaxPresentationsChange = (event) => {
      const value = event.target.value;
      setMaxPresentations(value);
    }

    const [maxAttendees, setMaxAttendees] = useState('');
    const handleMaxAttendeesChange = (event) => {
      const value = event.target.value;
      setMaxAttendees(value);
    }

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
      const value = event.target.value;
      setLocation(value);
    }

    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
            console.log(data);
            setLocations(data.locations);
            }
      }

        useEffect(() => {
        fetchData();
      }, []);



    return(
    <>
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartChange} value={start}  placeholder="Starts" type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndChange} value={end}  placeholder="Ends" type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea onChange={handleDescriptionChange} value={description} className="form-control" name="description" id="description" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentationsChange} value={maxPresentations}  placeholder="Maximum Presentations" type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_pre">Maximum presentations</label>
              </div>
                <div className="form-floating mb-3">
                    <input onChange={handleMaxAttendeesChange} value={maxAttendees}  placeholder="Maximum attendees" type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                    <label htmlFor="max_att">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} id="location" name="location" className="form-select">
                <option value="">Choose a location</option>
                    {locations.map(location => {
                        return (
                        <option key={location.id} value={location.id}>
                        {location.name}
                </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
  );

  }

export default ConferenceForm;
